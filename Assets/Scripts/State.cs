﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Creates a menu item in Asset Menu named "State" which is the scriptable object we have created with this script

[CreateAssetMenu(menuName = "State")]

// Scriptable Object Class
public class State : ScriptableObject {

    // Creates a TextArea component in the inspector which contains a string called "storyText"
    [TextArea(14,10)] [SerializeField] string storyText;
    [SerializeField] State[] nextStates;

    // When GetStateStory is called we return the string "storyText"
    public string GetStateStory()
    {
        return storyText;
    }

    public State[] GetNextStates()
    {
        return nextStates;
    }
}
