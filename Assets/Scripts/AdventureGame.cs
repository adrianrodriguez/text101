﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; // <- This is a namespace

public class AdventureGame : MonoBehaviour {

    // All Variables start with a small letter
    // [SerializedField] <- now have this variable available in our inspector
    [SerializeField] Text textComponent;

    // This adds a component to the game object in the inspector with the class of State (the scriptable object we created)
    [SerializeField] State startingState;

    // This variable links the script "State" so we can call it's methods/variables.
    State state;

    // Going over STATES and STATE machines
    // start -> state A -> decision -> state B or state C


    // Scriptable Objects (only information they have in them is the information we need)

	// Use this for initialization
	void Start () {
        // Whenever changing UI text always include '.text' (which is the property of the Text component after the variable

        state = startingState; // Now the state is going to = the starting state component of the object 

        // Now we call GetStateStory() method in State to return the string to use in our game 
        textComponent.text = state.GetStateStory();

	}
	
	// Update is called once per frame
	void Update () {

        // Managing our state

        ManageState();
	}

    // Only wanting to use this method in this particular class
    private void ManageState() {

        // Create variable that links to the array of next states
        var nextStates = state.GetNextStates();

        for (int index = 0; index < nextStates.Length; index++) {

            // index is going to be our position
            if (Input.GetKeyDown(KeyCode.Alpha1 + index))
            {
                // Numbers on the top of the keyboard are alphas
                state = nextStates[index];
            }

        }

        // Since the text story changes we have to call this method.
        textComponent.text = state.GetStateStory();
    }
}
